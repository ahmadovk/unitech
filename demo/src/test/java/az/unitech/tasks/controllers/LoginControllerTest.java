package az.unitech.tasks.controllers;

import az.unitech.tasks.config.AuthenticationFilter;
import az.unitech.tasks.config.JwtAuthenticationEntryPoint;
import az.unitech.tasks.config.JwtUtil;
import az.unitech.tasks.config.SecurityConfig;
import az.unitech.tasks.interfaces.ILoginService;
import az.unitech.tasks.models.entity.User;
import az.unitech.tasks.models.input.RegisterModel;
import az.unitech.tasks.models.output.GeneralResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Import(SecurityConfig.class)
@WebMvcTest(value = LoginController.class, includeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = JwtUtil.class)})
@ExtendWith(MockitoExtension.class)
class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ILoginService loginService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationFilter authenticationFilter;

    @MockBean
    private JwtAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private GeneralResponse<User> generalResponse;

    private RegisterModel registerModel;
    private RegisterModel registerModel1;

    @BeforeEach
    public void init () {
        registerModel = new RegisterModel("pin", "password");
        registerModel1 = new RegisterModel("111111", "password");
    }

    @Test
    void testSignUpSuccess() throws Exception {

        GeneralResponse<User> expectedResponse = new GeneralResponse<>(true, new User(2, "pin","password"), "SUCCESS");
        ResponseEntity<GeneralResponse<User>> responseEntity = ResponseEntity.ok().body(expectedResponse);

        when(loginService.register(ArgumentMatchers.any())).thenReturn(responseEntity);
        ResultActions response = mockMvc.perform(post("/fintech-ws/v1/login/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerModel)));
        response.andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk()) // Check that the status is OK
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.data.id").value(2))
                .andExpect(jsonPath("$.data.pin").value("pin"))
                .andExpect(jsonPath("$.message").value("SUCCESS"));

    }

    @Test
    void testSignUpFailure() throws Exception {

        GeneralResponse<User> expectedResponse = new GeneralResponse<>(false, new User(1, "111111","password"), "SUCCESS");
        ResponseEntity<GeneralResponse<User>> responseEntity = ResponseEntity.badRequest().body(expectedResponse);

        when(loginService.register(ArgumentMatchers.any())).thenReturn(responseEntity);

        ResultActions response = mockMvc.perform(post("/fintech-ws/v1/login/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerModel1)));

        response.andDo(MockMvcResultHandlers.print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.success").value(false));
    }
}
