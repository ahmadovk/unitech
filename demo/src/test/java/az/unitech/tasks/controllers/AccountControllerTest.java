package az.unitech.tasks.controllers;

import az.unitech.tasks.config.JwtAuthenticationEntryPoint;
import az.unitech.tasks.config.JwtUtil;
import az.unitech.tasks.config.SecurityConfig;
import az.unitech.tasks.interfaces.IAccountService;
import az.unitech.tasks.models.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(SecurityConfig.class)
@ExtendWith(SpringExtension.class)
@WebMvcTest(value = AccountController.class, includeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = JwtUtil.class)})
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JwtUtil jwtUtil;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @MockBean
    private IAccountService accountService;



    @Test
    void testGetAccounts() throws Exception {

        String jwtToken = jwtUtil.generateToken(new User(1,"1","password"));

        ResultActions response = mockMvc.perform(get("/fintech-ws/v1/account")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken)
                .accept(MediaType.APPLICATION_JSON));
        response.andDo(MockMvcResultHandlers.print());
        response.andExpect(status().isOk());

    }

    @Test
    void testTransferAmount() throws Exception {

        String jwtToken = jwtUtil.generateToken(new User(1,"1","password"));

        ResultActions response = mockMvc.perform(get("/fintech-ws/v1/account")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken)
                        .param("principalAccountId","1")
                        .param("beneficiaryAccountId","2")
                        .param("amount", "1")
                .accept(MediaType.APPLICATION_JSON));
        response.andDo(MockMvcResultHandlers.print());

        response.andExpect(status().isOk());

    }
}
