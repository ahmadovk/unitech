package az.unitech.tasks.controllers;

import az.unitech.tasks.config.AuthenticationFilter;
import az.unitech.tasks.config.JwtAuthenticationEntryPoint;
import az.unitech.tasks.config.JwtUtil;
import az.unitech.tasks.config.SecurityConfig;
import az.unitech.tasks.interfaces.ICurrencyService;
import az.unitech.tasks.models.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import(SecurityConfig.class)
@WebMvcTest(value = CurrencyController.class, includeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = JwtUtil.class)})
@ExtendWith(MockitoExtension.class)
class CurrencyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ICurrencyService currencyService;

    @Autowired
    private JwtUtil jwtUtil;

    @MockBean
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private AuthenticationFilter authenticationFilter;

    @Test
    void testGetRateSuccess() throws Exception {

        String jwtToken = jwtUtil.generateToken(new User(1,"1","password"));
        ResultActions response = mockMvc.perform(get("/fintech-ws/v1/currency/USD")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken)
                .accept(MediaType.APPLICATION_JSON));
        response.andDo(MockMvcResultHandlers.print());

        response.andExpect(status().isOk());
    }

    @Test
    void testGetRateFailure() throws Exception {
        String jwtToken = jwtUtil.generateToken(new User(1,"1","password"));
        given(currencyService.getRate("1213")).willReturn(ResponseEntity.badRequest().build());

        ResultActions response = mockMvc.perform(get("/fintech-ws/v1/currency/1213")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + jwtToken));
        response.andDo(MockMvcResultHandlers.print());
        response.andExpect(status().isBadRequest());

    }
}