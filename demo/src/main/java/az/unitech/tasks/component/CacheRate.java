package az.unitech.tasks.component;

import az.unitech.tasks.models.ext.ExchangeRate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class CacheRate {

    private static final LoadingCache<String, ExchangeRate> exchangeRateCache = CacheBuilder.newBuilder()
            .expireAfterWrite(60, TimeUnit.SECONDS)
            .build(new CacheLoader<>() {
                @NotNull
                @Override
                public ExchangeRate load(@NotNull String key) {
                    return new ExchangeRate(null,null);
                }
            });

    public static Double getQuoteCurrencyRateFromCache(String key) {
        try {
            return exchangeRateCache.get(key).getRate();
        } catch (ExecutionException e) {
            log.error("Error fetching URL from cache for key {}: {}", key, e.getMessage());
            return null;
        }
    }


    public static void putQuoteCurrencyRateInCache(String key, ExchangeRate exchangeRate) {
        Thread cacheThread = new Thread(() -> {
            log.info("Putting minio pre signed url in cache for key {}", key);
            exchangeRateCache.put(key, exchangeRate);
        });
    }
}
