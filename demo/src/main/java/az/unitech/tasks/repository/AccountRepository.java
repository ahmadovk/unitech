package az.unitech.tasks.repository;

import az.unitech.tasks.models.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account,Integer> {

    Optional<Account> findByIdAndIsActiveIsTrue(int accountId);
    List<Account> findByUserId(int userId);

}
