package az.unitech.tasks.controllers;

import az.unitech.tasks.interfaces.IAccountService;
import az.unitech.tasks.models.input.RegisterModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@CrossOrigin(origins = "*")
@Slf4j
@AllArgsConstructor
@RequestMapping("/fintech-ws/v1/account")
public class AccountController {

    private final IAccountService accountService;

    @SecurityRequirement(name = "Bearer Authentication")
    @GetMapping
    @Operation(summary = "This service will be used for users to get accounts")
    public ResponseEntity<?> getAccounts() {
        return accountService.getAccounts();
    }

    @SecurityRequirement(name = "Bearer Authentication")
    @PostMapping("/transfer")
    @Operation(summary = "This service will be used for users to transfer amount between accounts")
    public ResponseEntity<?> transferAmount(@RequestParam int principalAccountId,
                                            @RequestParam int beneficiaryAccountId,
                                            @RequestParam BigDecimal amount) {
        return accountService.transferAmount(principalAccountId,beneficiaryAccountId,amount);
    }
}
