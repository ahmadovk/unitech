package az.unitech.tasks.controllers;

//import az.unitech.tasks.interfaces.ILoginService;
import az.unitech.tasks.interfaces.ILoginService;
import az.unitech.tasks.models.entity.User;
import az.unitech.tasks.models.input.LoginModel;
import az.unitech.tasks.models.input.RegisterModel;
import az.unitech.tasks.models.output.GeneralResponse;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@Slf4j
@AllArgsConstructor
@RequestMapping("/fintech-ws/v1/login")
public class LoginController {
    private final ILoginService loginService;

    @PostMapping("/register")
    @Operation(summary = "This service will be used for users who wants to register with pin.")
    public ResponseEntity<?>signUp(@Validated @RequestBody RegisterModel request) {
        log.info("request is : {}", request.getPin());
        return loginService.register(request);
    }
    @PostMapping("/sign-in")
    @Operation(summary = "This service will be used for users to be logged  with pin and password.")
    public ResponseEntity<?> signIn(@RequestBody LoginModel request) {
        return loginService.login(request);
    }
}
