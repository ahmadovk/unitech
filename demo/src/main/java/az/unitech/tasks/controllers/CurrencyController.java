package az.unitech.tasks.controllers;

import az.unitech.tasks.interfaces.ICurrencyService;
import az.unitech.tasks.services.CurrencyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@Slf4j
@AllArgsConstructor
@RequestMapping("/fintech-ws/v1/currency")
public class CurrencyController {

    private final ICurrencyService currencyService;

    @SecurityRequirement(name = "Bearer Authentication")
    @GetMapping("/{currency}")
    @Operation(summary = "This service will be used for users to get rate based on quote currency code")
    public ResponseEntity<?> getRate(@PathVariable String currency) {
        return currencyService.getRate(currency);
    }
}
