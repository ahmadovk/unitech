package az.unitech.tasks.interfaces;

import az.unitech.tasks.models.entity.User;
import az.unitech.tasks.models.input.LoginModel;
import az.unitech.tasks.models.input.RegisterModel;
import az.unitech.tasks.models.output.GeneralResponse;
import org.springframework.http.ResponseEntity;

public interface ILoginService {
    ResponseEntity<GeneralResponse<User>> register(RegisterModel request);

    ResponseEntity<GeneralResponse<Object>>login(LoginModel request);
}
