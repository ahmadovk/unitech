package az.unitech.tasks.interfaces;

import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;

public interface IAccountService {
    ResponseEntity<?> getAccounts();

    ResponseEntity<?> transferAmount(int principalAccountId, int beneficiaryAccountId, BigDecimal amount);
}
