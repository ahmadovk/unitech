package az.unitech.tasks.interfaces;

import org.springframework.http.ResponseEntity;

public interface ICurrencyService {
    ResponseEntity<?> getRate(String quoteCurrency);
}
