package az.unitech.tasks.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class SecurityService {

    public Authentication getAuthentication() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.isNull(authentication))
            throw new IllegalArgumentException("Authentication is null");

        return authentication;
    }

    public UserPrincipal getUserPrincipal() {
        Authentication authentication = getAuthentication();
        return getUserPrincipal(authentication);
    }


    public UserPrincipal getUserPrincipal(Authentication authentication) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        if (userPrincipal == null)
            throw new IllegalArgumentException("Authentication has no user id");
        return userPrincipal;
    }

}
