package az.unitech.tasks.models.input;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class RegisterModel {

    private String pin;
    private String password;
}
