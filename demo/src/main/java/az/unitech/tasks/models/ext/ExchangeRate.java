package az.unitech.tasks.models.ext;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ExchangeRate {
    private Double rate;
    private LocalDateTime expiration;
}
