package az.unitech.tasks.models.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
public class ExchangeResponse {
        String baseCurrency;
        String quoteCurrency;
        Double rate;

}
