package az.unitech.tasks.models.input;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginModel {

    private String pin;
    private String password;

}
