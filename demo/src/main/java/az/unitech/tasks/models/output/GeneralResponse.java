package az.unitech.tasks.models.output;


import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class GeneralResponse<T> implements IGeneralResponse<T> {
    private boolean success;
    private T data;
    private String message;

    @Override
    public T getData() {
        return this.data;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public Boolean getSuccess() {
        return this.success;
    }
}
