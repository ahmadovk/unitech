package az.unitech.tasks.models.output;

public interface IGeneralResponse<T> {
    Boolean getSuccess();
    T getData();
    String getMessage();
}
