package az.unitech.tasks.services;

import az.unitech.tasks.config.SecurityService;
import az.unitech.tasks.config.UserPrincipal;
import az.unitech.tasks.interfaces.IAccountService;
import az.unitech.tasks.models.entity.Account;
import az.unitech.tasks.models.output.GeneralResponse;
import az.unitech.tasks.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class AccountService implements IAccountService {

    private final SecurityService securityService;

    private final AccountRepository accountRepository;
    @Override
    public ResponseEntity<?> getAccounts() {
        UserPrincipal userPrincipal = (UserPrincipal) securityService.getAuthentication().getPrincipal();
        List<Account> accounts = accountRepository.findByUserId(userPrincipal.getId());

        return ResponseEntity.ok(new GeneralResponse<>(true, accounts, "Accounts of user returned"));
    }

    @Override
    public ResponseEntity<?> transferAmount(int principalAccountId, int beneficiaryAccountId, BigDecimal amount) {
        try {
            UserPrincipal userPrincipal = (UserPrincipal) securityService.getAuthentication().getPrincipal();
            Optional<Account> accountOptional = accountRepository.findByIdAndIsActiveIsTrue(principalAccountId);
            if (principalAccountId == beneficiaryAccountId)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new GeneralResponse<>(false, null, "You can not transfer amount between same accounts"));

            if (accountOptional.isEmpty())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new GeneralResponse<>(false, null, "Provided account is not correct"));
            // Get beneficiaryAccount
            Optional<Account> beneficiaryOptional = accountRepository.findByIdAndIsActiveIsTrue(beneficiaryAccountId);
            if (beneficiaryOptional.isEmpty())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new GeneralResponse<>(false, null, "Provided beneficiary account is not correct"));
            if (!beneficiaryOptional.get().isActive())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new GeneralResponse<>(false, null, "Provided beneficiary account is deactivate"));

            if (accountOptional.get().getUserId() != userPrincipal.getId())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new GeneralResponse<>(false, null, "Account does not belong to user"));

            if (accountOptional.get().getBalance().compareTo(amount) < 0)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new GeneralResponse<>(false, null, "Not enough balance in account"));

            // Transfer amount (update balance)
            accountOptional.get().setBalance(accountOptional.get().getBalance().subtract(amount));
            accountRepository.save(accountOptional.get());

            beneficiaryOptional.get().setBalance(beneficiaryOptional.get().getBalance().add(amount));
            accountRepository.save(beneficiaryOptional.get());

            return ResponseEntity.ok(new GeneralResponse<>(true, null, "Amount transferred successfully"));
        } catch (Exception e) {
            log.error("Exception: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    new GeneralResponse<>(false, null, "Something went wrong."));
        }
    }
}
