package az.unitech.tasks.services;

import az.unitech.tasks.component.CacheRate;
import az.unitech.tasks.interfaces.ICurrencyService;
import az.unitech.tasks.models.ext.ExchangeRate;
import az.unitech.tasks.models.output.GeneralResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class CurrencyService implements ICurrencyService {
    @Override
    public ResponseEntity<?> getRate(String quoteCurrency) {
        try {

            log.info("quoteCurrency: {}",quoteCurrency);
            // Base always AZN
            Double rate = CacheRate.getQuoteCurrencyRateFromCache(quoteCurrency);
            if (rate != null)
                return ResponseEntity.status(HttpStatus.OK).body(
                        new GeneralResponse<>(true, rate, "SUCCESS"));

            // else call third party service and save to cache

            Double exchangeRate = ExchangeClient.getExchangeRate(quoteCurrency);

            // save to cache

            CacheRate.putQuoteCurrencyRateInCache(quoteCurrency, new ExchangeRate(exchangeRate, LocalDateTime.now()));
            if(exchangeRate == 0.0)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        new GeneralResponse<>(false,null, "Request is not correct"));

            return ResponseEntity.status(HttpStatus.OK).body(
                    new GeneralResponse<>(true, exchangeRate, "SUCCESS"));


        }catch (Exception e){
            log.error("Exception: {} ",e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }
}
