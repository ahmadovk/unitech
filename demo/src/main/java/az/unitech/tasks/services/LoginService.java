package az.unitech.tasks.services;

//import az.unitech.tasks.config.JwtUtil;
import az.unitech.tasks.config.JwtUtil;
import az.unitech.tasks.interfaces.ILoginService;
import az.unitech.tasks.models.entity.User;
import az.unitech.tasks.models.input.LoginModel;
import az.unitech.tasks.models.input.RegisterModel;
import az.unitech.tasks.models.output.GeneralResponse;
import az.unitech.tasks.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class LoginService implements ILoginService {

    private final UserRepository userRepository;

    private final JwtUtil jwtUtil;

    private final PasswordEncoder passwordEncoder;
    @Override
    public ResponseEntity<GeneralResponse<User>> register(RegisterModel request) {
        try {
            log.info("register started");
            Optional<User> userOptional = userRepository.findByPin(request.getPin());
            if (userOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(new GeneralResponse<>(false, null, "Pin already exists"));
            }
            User newUser = userRepository.save(User.builder()
                    .pin(request.getPin())
                    .password(passwordEncoder.encode(request.getPassword()))
                    .build());
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new GeneralResponse<>(true, newUser, "SUCCESS"));
        } catch (Exception e) {
            log.error("Exception: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GeneralResponse<>(false, null, "Internal server error"));
        }
    }

    @Override
    public ResponseEntity<GeneralResponse<Object>> login(LoginModel request) {
        try {
            Optional<User> userOptional = userRepository.findByPin(request.getPin());
            if (userOptional.isPresent()) {
                if(passwordEncoder.matches(request.getPassword(), userOptional.get().getPassword())) {
                    String token = jwtUtil.generateToken(userOptional.get());
                    return ResponseEntity.status(HttpStatus.OK).body(
                            new GeneralResponse<>(true, token, "SUCCESS"));
                }
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                    new GeneralResponse<>(false, null, "Pin or password is not correct"));

        }catch (Exception e){
            log.error("Exception: {}",e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    new GeneralResponse<>(false, null, "Something went wrong."));
        }
    }
}
