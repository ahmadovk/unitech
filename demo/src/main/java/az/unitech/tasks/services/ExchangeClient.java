package az.unitech.tasks.services;

import az.unitech.tasks.models.output.ExchangeResponse;

import java.util.HashMap;
import java.util.Map;

public class ExchangeClient {

    private static final Map<String, Double> exchangeRates = new HashMap<>();

    static {
        exchangeRates.put("USD", 1.71);
        exchangeRates.put("EUR", 1.91);
    }

    public static Double getExchangeRate(String quoteCurrency) {

        return exchangeRates.getOrDefault(quoteCurrency, 0.0);


        // This will be used to call third party service

//        WebClient webClient = WebClient.builder()
//                .baseUrl(paymentUrl)
//                .defaultHeader(HttpHeaders.AUTHORIZATION, secret)
//                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
//                .build();
//
//        Mono<ExchangeResponse> responseMono = webClient
//                .post()
//                .uri("getRate")
//                .contentType(MediaType.APPLICATION_JSON)
//                .body(Mono.just(rate), Exchange.class)
//                .retrieve()
//                .bodyToMono(ExchangeResponse.class);
    }
}
