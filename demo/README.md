# Fintech Application

This is a Spring Boot application for the Fintech project.

## Setup

### Prerequisites

- Java Development Kit (JDK) 11 or higher installed
- Maven installed
- PostgreSQL database server installed

### Database Setup

1. Create a new database named `fintech`:
2. Create a new user named `fintech_user` with password `fintech_user@` and grant superuser privileges:
3. Build the application using Maven:

## Transfer Funds between Accounts (Balance is not enough)
URL: http://localhost:8081/fintech-ws/v1/account/transfer?principalAccountId=1&beneficiaryAccountId=2&amount=500
Method: POST
Description: Transfers funds from one account to another.
Request Parameters:
principalAccountId: ID of the account from which funds are being transferred (e.g., 1).
beneficiaryAccountId: ID of the account to which funds are being transferred (e.g., 2).
amount: Amount of funds to transfer (e.g., 500).
Response Status: 400 Bad Request
Response Body:
    {
    "success": false,
    "data": null,
    "message": "Not enough balance in account"
    }

## Transfer Funds between Accounts (Account is not correct)
URL: http://localhost:8081/fintech-ws/v1/account/transfer?principalAccountId=1&beneficiaryAccountId=5&amount=500
Method: POST
Description: Transfers funds from one account to another.
Request Parameters:
principalAccountId: ID of the account from which funds are being transferred (e.g., 1).
beneficiaryAccountId: ID of the account to which funds are being transferred (e.g., 5).
amount: Amount of funds to transfer (e.g., 500).
Response Status: 400 Bad Request
Response Body:
    {
    "success": false,
    "data": null,
    "message": "Provided beneficiary account is not correct"
    }